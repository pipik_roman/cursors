/**
 * 
 */
package sk.piro.ui.cursor;

import java.awt.Cursor;
import java.awt.FlowLayout;

import javax.swing.JFrame;

import sk.piro.ui.cursor.awt.CursorFactory;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CursorTest {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		Cursor cursor = CursorFactory.getInstance().get(CursorType.CROSSHAIR);
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().setCursor(cursor);
		frame.setCursor(cursor);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(200, 200);
		frame.getRootPane().setCursor(cursor);
		frame.setVisible(true);
	}
}
