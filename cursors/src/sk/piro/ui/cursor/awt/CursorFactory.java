/**
 * 
 */
package sk.piro.ui.cursor.awt;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import sk.piro.ui.cursor.AbstractCursorFactory;
import sk.piro.ui.cursor.CursorType;

/**
 * Cursor factory for AWT cursors
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CursorFactory extends AbstractCursorFactory<Cursor, BufferedImage> {
	private static final Toolkit toolkit = Toolkit.getDefaultToolkit();
	private static final CursorFactory INSTANCE = new CursorFactory();

	private CursorFactory() {
	}

	public static final CursorFactory getInstance() {
		return INSTANCE;
	}

	@Override
	protected final Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	@Override
	protected Cursor createDefault(CursorType cursorType) {
		switch (cursorType) {
			case DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
			case WAIT_DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
			case MOVE_DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
			case HAND_DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
			case CROSSHAIR_DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
			case TEXT_DEFAULT:
				return Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR);
			default:
				throw new AssertionError("Unkown default cursor type: " + cursorType);
		}

	}

	@Override
	protected BufferedImage createCursorImage(CursorType cursorType, String style) {
		String imageName = "sk/piro/ui/cursor/" + cursorType.getName() + style + ".png";
		ClassLoader loader = AbstractCursorFactory.class.getClassLoader();
		try {
			return ImageIO.read(loader.getResourceAsStream(imageName));
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected Cursor createCustom(CursorType cursorType, String style, BufferedImage image) {
		return toolkit.createCustomCursor(image, new Point(cursorType.getX(), cursorType.getY()), cursorType.getName());
	}
}
