/**
 * 
 */
package sk.piro.ui.cursor;

/**
 * Cursor type as enumeration
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public enum CursorType implements CursorI {
	CROSSHAIR_DEFAULT("crosshair"), CROSSHAIR("crosshair", 16, 16), DEFAULT("default"), HAND_DEFAULT("hand"), HAND("hand", 14, 9), MOVE_DEFAULT("move"), MOVE(
			"move", 16, 16), MOVE_VERTICAL("move.vertical", 16, 16), MOVE_HORIZONTAL("move.horizontal", 16, 16), MOVE_DIAGONAL("move.diagonal", 16, 16), MOVE_DIAGONAL_NWSE(
			"move.diagonal.nwse", 16, 16), MOVE_DIAGONAL_SWNE("move.diagonal.swne", 16, 16), ROTATE("rotate", 16, 16), ROTATE_CLOCKWISE("rotate.clockwise", 16,
			16), ROTATE_COUNVER_CLOCKWISE("rotate.counter_clockwise", 16, 16), SQUARE("square", 16, 16), TEXT_DEFAULT("text"), TRANSPARENT("transparent", 0, 0), WAIT_DEFAULT(
			"wait");

	private final String name;
	private final boolean isDefault;
	private final int x;
	private final int y;

	private CursorType(String name) {
		this(name, true, 0, 0);
	}

	private CursorType(String name, int x, int y) {
		this(name, false, x, y);
	}

	private CursorType(String name, boolean isDefault, int x, int y) {
		this.name = name;
		this.isDefault = isDefault;
		this.x = x;
		this.y = y;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public boolean isDefault() {
		return this.isDefault;
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	// dalsie kurzory z corel draw: lupa, lupa +, lupa -, ruka posuvajuca (nie ako hand ktory mame teraz), fill, pick color, guma,custom wait

}
