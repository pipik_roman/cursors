/**
 * 
 */
package sk.piro.ui.cursor;

/**
 * Cursor interface
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface CursorI {
	/**
	 * Name for cursor
	 * 
	 * @return name for cursor
	 */
	public String getName();

	/**
	 * Returns true if cursor should be default by environment
	 * 
	 * @return true if cursor should be default by environment
	 */
	public boolean isDefault();

	/**
	 * @return x coordinate of hot spot
	 */
	public int getX();

	/**
	 * @return y coordinate of hot spot
	 */
	public int getY();
}
