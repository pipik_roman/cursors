/**
 * 
 */
package sk.piro.ui.cursor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Cursor>
 *            class for cursor implementation
 * @param <ImageType>
 *            class for image
 */
public abstract class AbstractCursorFactory<Cursor, ImageType> {

	private String style = "";
	private Map<CursorType, Cursor> cursors = new HashMap<CursorType, Cursor>();

	public Cursor get(CursorType cursorType) {
		Cursor cursor = cursors.get(cursorType);
		if (cursor == null) {
			cursor = create(cursorType);
		}
		return cursor;
	}

	/**
	 * @return style used for cursors
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * This will set a new style and clear chache with created cursors
	 * 
	 * @param style
	 *            style to use for cursors
	 */
	public void setStyle(String style) {
		this.style = style;
		cursors.clear();
	}

	private Cursor create(CursorType cursorType) {
		final Cursor c;
		if (cursorType.isDefault()) {
			c = createDefault(cursorType);
		}
		else {
			c = createCursor(cursorType);
		}
		cursors.put(cursorType, c);
		return c;
	}

	private Cursor createCursor(CursorType cursorType) {
		ImageType image = createCursorImage(cursorType, style);
		return createCustom(cursorType, style, image);
	}

	/**
	 * Create default cursor
	 * 
	 * @param cursorType
	 *            type of cursor
	 * @return created cursor
	 */
	protected abstract Cursor createDefault(CursorType cursorType);

	/**
	 * Create image for cursor
	 * 
	 * @param cursorType
	 *            type of cursor
	 * @param style
	 *            style to use
	 * @return ImageType image for cursor
	 */
	protected abstract ImageType createCursorImage(CursorType cursorType, String style);

	/**
	 * Create custom cursor
	 * 
	 * @param cursorType
	 *            type of cursor
	 * @param style
	 *            style to use
	 * @param image
	 *            image for cursor
	 * @return created cursor
	 */
	protected abstract Cursor createCustom(CursorType cursorType, String style, ImageType image);

}
